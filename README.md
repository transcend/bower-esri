# ESRI Module
The "ESRI" module provides a set of reusable components to quickly and easily create components based on the [ArcGIS
JavaScript API](https://developers.arcgis.com/javascript/). The main benefit of the components available in this module
is the integration they have with other AngularJS components. This component integration allows binding of map values
like zoom level, center location, layers, etc. This two-way binding allows many components to all share the same values
and remain in sync automatically.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-esri.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-esri/get/master.zip](http://code.tsstools.com/bower-esri/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-esri](http://code.tsstools.com/bower-esri)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.esri']);
```

## To Do
- Find a way to mock the dojo and esri libraries in unit tests.