/**
 * @ngdoc overview
 * @name transcend.esri
 * @description
 # ESRI Module
 The "ESRI" module provides a set of reusable components to quickly and easily create components based on the [ArcGIS
 JavaScript API](https://developers.arcgis.com/javascript/). The main benefit of the components available in this module
 is the integration they have with other AngularJS components. This component integration allows binding of map values
 like zoom level, center location, layers, etc. This two-way binding allows many components to all share the same values
 and remain in sync automatically.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-esri.git
 ```

 Other options:

 * Download the code at [http://code.tsstools.com/bower-esri/get/master.zip](http://code.tsstools.com/bower-esri/get/master.zip)
 * View the repository at [http://code.tsstools.com/bower-esri](http://code.tsstools.com/bower-esri)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.esri']);
 ```

 ## To Do
 - Find a way to mock the dojo and esri libraries in unit tests.
 */
/**
 * @ngdoc object
 * @name transcend.esri.esriConfig
 *
 * @description
 * Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any
 * components in the {@link transcend.esri Esri Module}. The "esriConfig" object is a
 * {@link http://docs.angularjs.org/api/auto/object/$provide#value module value} and can therefore be
 * overridden/configured by:
 *
 <pre>
 // Override the value of the 'esriConfig' object.
 angular.module('myApp').value('esriConfig', {
     basemapSelector: {
       map: 'streets'
     }
   });
 </pre>
 **/
/**
 * @ngdoc directive
 * @name transcend.esri.directive:basemapSelector
 *
 * @description
 * The 'basemapSelector' directive displays a list of available basemaps for the current application session and
 * changes the display if a different basemap than the currenly selected one is clicked.
 *
 * @restrict EA
 * @element ANY
 * @replace true
 *
 * @requires esriConfig
 *
 * @param {string} basemap The basemap active for the current session. Valid options are: "hybrid", "streets",
 * "satellite", "topo", "gray", "oceans", "national-geographic", "osm"
 *
 * @param {function} onChange The method used to change the basemap in the map display.
 *
 * @example
 <example module="app">
 <file name="index.html">
 <link rel="stylesheet" href="http://js.arcgis.com/3.7/js/esri/css/esri.css">
 <div ng-controller="Ctrl">
 <esri-map center="center" zoom="zoom" style="position:relative">
 <div style="position:absolute; left: 10px; bottom: 10px;">
 <basemap-selector basemap="selection.basemap"></basemap-selector>
 </div>
 </esri-map>

 <hr>
 <label>
 Basemap:
 <select ng-model="selection.basemap" ng-options="bm for bm in basemaps"></select>

 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.esri'])
 .controller('Ctrl', function($scope) {
        $scope.selection = {};
        $scope.basemaps = ['hybrid', 'streets', 'satellite', 'topo', 'gray', 'oceans', 'national-geographic', 'osm'];
      });
 </file>
 </example>
 */
/**
 * @ngdoc service
 * @name transcend.esri.factory:$date
 *
 * @description
 * The '$date' factory provides multiple methods of modifying the Date into the necessary format for querying or display.
 *
 * @requires $filter
 * @requires transcend.core.$string
 **/
/**
     * @ngdoc method
     * @name transcend.esri.factory:$date#isNumericString
     * @propertyOf transcend.esri.factory:$date
     *
     * @description
     * Tests if the date value is a numeric string
     *
     * @param {String} dateValue the time in milliseconds
     *
     * @returns {boolean} flag whether or not dateValue is numeric String
     */
/**
     * @ngdoc method
     * @name transcend.esri.factory:$date#isNumericString
     * @propertyOf transcend.esri.factory:$date
     *
     * @description
     * Changes numeric String dateValue to a number.
     *
     * @param {String} dateValue the time in milliseconds
     *
     * @returns {Number} formatted dateValue
     */
/**
     * @ngdoc method
     * @name transcend.esri.$toDate
     * @propertyOf transcend.esri.factory:$date
     *
     * @description
     * creates Date object of with a value of dateValue
     *
     * @param {String} dateValue the time in milliseconds
     *
     * @returns {*} modified dateValue
     */
/**
     * @ngdoc method
     * @name transcend.esri.$toMilliseconds
     * @propertyOf transcend.esri.factory:$date
     *
     * @description
     * creates milliseconds value from a date object
     *
     * @param {String} dateValue the date object
     *
     * @returns {*} modified dateValue time
     */
/**
 * @ngdoc filter
 * @name transcend.esri.filter:utcDate
 *
 * @description
 The 'utcDate' filter modifies a given date into the UTC format.
 *
 * @requires $filter
 * @requires $date
 *
 *
 */
/**
 * @ngdoc filter
 * @name transcend.esri.filter:featureOrderBy
 *
 * @description
 The 'featureOrderBy' filter orders a given feature set by a selected feature.
 *
 *
 *
 */
/**
 * @ngdoc directive
 * @name transcend.esri.directive:hierarchyFilter
 *
 * @description
 The 'hierarchyFilter' directive provides an editable template for the Search By Hierarchy Tool
 *
 * @restrict EA
 * @scope
 *
 * @requires $timeout
 * @requires tsConfig
 * @requires $loading
 * @requires transcend.core.$notify
 * @requires $http
 * @requires $layer
 * @requires MapServer
 * @requires $filter
 */
/**
         * Internal list of where clauses that currently represent the state of the hierarchy filter.
         * Essentially each select in the hierarchy represents a single where clause in the list.
         * @type {Array}
         */
/**
           * Flag to keep track if this directive has been "initialized" yet. When the directive initializes it
           * will make the call to go get the data for the hierarchy selects.
           * @type {boolean}
           */
/**
           * Error handler/callback for asynchronous calls.
           */
/**
           * Number of items currently loading.
           * @type {number}
           */
/**
           * Called when an item begins loading.
           */
/**
           * Called when an item ends loading.
           */
/**
           * Initialization function that will be setup the directive when it is created (or told to be initialized).
           * This function will only add the watches and retrieve the data once - unless the directive is created again.
           * @returns {boolean}
           */
/**
           * Function to get a single where clause string based on the current state of the hierarchy filter.
           * This function will also trigger the setting of the 'where' on the scope if the attribute was set/passed in.
           * @param where {string} Optional.
           * @returns {string}
           */
/**
           * Automatically selects the first (or default) option if there is only one option available.
           * @param item
           */
/**
           * Retrieves and sets the aliases for the data in this item.
           * @param item
           */
/**
           * Goes through each feature and aliases the value if there are any coded value domains.
           * @param {object} item The hierarchy filter item to set aliases for.
           */
/**
           * Retrieves the unique values for a given hierarchy item (example: all counties, directions, etc). Note that this
           * query will use the current where clause/state of the hierarchy filter - which creates the hierarchy effect.
           * @param item {Object} Optional. The item to get the data for. If it is null it will default to the first filter.
           * @returns {boolean} Whether the data retrieval process happened or not.
           */
/**
           * Analyzes the current state of the hierarchy selection and retrieves the appropriate data if needed.
           * This function is typically called after hierarchy selection has changed.
           */
/**
         * Fired when a select changes which means we want to clear any values downstream of the changed item.
         * @param index
         */
/**
         * Clears all the selects and restores the hierarchy filter to its original state.
         */
/**
         * Returns the placeholder text for a particular item/select. The purpose of this functinoality is to show
         * some loading text while the select is loading rather than the normal 'Select an Item' placeholder text.
         * @param item
         * @returns {string}
         */
/**
         * Function to kick off the 'onApply' function passed in from the user (if they set the attribute).
         */
/**
 * @ngdoc service
 * @name transcend.esri.service:$geometry
 *
 * @description
 * Provides a set of geometry utility functions.  The purpose of this service is to provide the necessary geometry
 * functionality for interacting with an esri map.
 *
 * @requires $filter
 * @requires esri
 * @requires $object
 *
 <h3>Calling Static $geometry Utility Functions</h3>
 <pre>
 // Create a line from an array of points
 var line = $geometry.polyline([pt1, pt2, pt3]);

 //Create a spatial reference based on a wkid value
 var ref = $geometry.spatialReference({wkid: value});
 </pre>
 *
 * See {@link http://docs.angularjs.org/api/ngResource/service/$resource AngularJS's $resource} documentation for
 * details on what methods are natively available to a resource.
 */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#$geometry
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Convert the given item into an appropriate esri geometry using the given spatial reference.  Note:
       * this method can handle a feature object as well as an esri geometry object.
       * @param {Object} geometry Item to be converted.
       * @param {Object} spatialReference The spatial reference to apply to the geometry
       * @returns {Object} The esri geometry for the item that was passed in.
       *
       * @example
       <pre>
       // Returns the geometry of the line
       var line = new esri.geometry.Polyline([{x:5, y:10}{x:30, y:50}]);
       var ref = new SpatialReference({wkid: 102100});
       var geom = $geometry(line, ref);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#isPolygon
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Determines whether or not a specified geometry is a polygon.
       * @param {Object} geometry An esri geometric object.
       * @returns {bool} True if the geometry is a polygon; false otherwise
       *
       * @example
       <pre>
       expect($geometry.isPolygon(new esri.geometry.Polygon())).toBeTruthy();
       expect($geometry.isPolygon(new esri.geometry.Polyline())).toBeFalsy();
       expect($geometry.isPolygon(new esri.geometry.Point())).toBeFalsy();
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#isPolyline
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Determines whether or not a specified geometry is a polyline.
       * @param {Object} geometry An esri geometric object.
       * @returns {bool} True if the geometry is a polyline; false otherwise
       *
       * @example
       <pre>
       expect($geometry.isPolyline(new esri.geometry.Polyline())).toBeTruthy();
       expect($geometry.isPolyline(new esri.geometry.Polygon())).toBeFalsy();
       expect($geometry.isPolyline(new esri.geometry.Point())).toBeFalsy();
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#isPoint
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Determines whether or not a specified geometry is a point.
       * @param {Object} geometry An esri geometric object.
       * @returns {bool} True if the geometry is a point; false otherwise
       *
       * @example
       <pre>
       expect($geometry.isPoint(new esri.geometry.Point())).toBeTruthy();
       expect($geometry.isPoint(new esri.geometry.Polygon())).toBeFalsy();
       expect($geometry.isPoint(new esri.geometry.Polyline())).toBeFalsy();
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#isSpatialReference
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Determines whether or not a specified parameter is a spatial reference.
       * @param {Object} spatialReference Any object.
       * @returns {bool} True if the object is a spatial reference; false otherwise
       *
       * @example
       <pre>
       expect($geometry.isSpatialReference(new esri.geometry.SpatialReference())).toBeTruthy();
       expect($geometry.isSpatialReference(new esri.geometry.Polygon())).toBeFalsy();
       expect($geometry.isSpatialReference(new esri.geometry.Point())).toBeFalsy();
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#spatialReference
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Create a spatial reference out of a passed in object.  If the forceCreate parameter is true, then create
       * the spatial reference whether or not the object passed contains valid spatial reference data.  If the
       * forceCreate parameter is false, then only create a spatial reference if the passed in object has valid
       * spatial reference information.
       * @param {object} spatialReference An object from which to create a spatial reference
       * @param {bool} forceCreate Whether or not to force a spatial reference to be created
       * @returns {Object|esri.geometry.SpatialReference} An esri spatial reference object
       *
       * @example
       <pre>
       // Should create a valid spatial reference object
       expect($geometry.spatialReference(new esri.geometry.SpatialReference())).toBeTruthy();
       // Should create a default spatial reference object
       expect($geometry.spatialReference(new esri.geometry.Polygon(), true)).toBeTruthy();
       // Should not create a spatial reference object
       expect($geometry.isSpatialReference(new esri.geometry.Point(), false)).toBeFalsy();
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#point
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Creates an esri.geometry.Point based on the passed in point object. If
       * an esri.geometry.Point is passed in, it will simply return the same object.
       * @param {Object} geometry An esri point or javascript object.
       * @param {Object} spatialReference The spatial reference to set the point to.
       * @returns {Object|esri.geometry.Point=} An esri.geometry.Point.
       *
       * @example
       <pre>
       var mapPoint = { 'x':-76, 'y':4e2 };
       // Returns the point that was passed in, in the specified spatial reference.
       $geometry.point(mapPoint, {wkid: 102100});
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#polyline
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Creates an esri.geometry.Polyline based on the passed in geometry object. If
       * an esri.geometry.Polyline is passed in, it will simply return the same object.
       * @param {Object} geometry An esri polyline or javascript object.
       * @param {Object} spatialReference The spatial reference to set the polyline to.
       * @returns {Object|esri.geometry.Polyline=} An esri.geometry.Polyline.
       *
       * @example
       <pre>
       var mapPoint1 = { 'x':-76, 'y':4e2 },
       mapPoint2 = { 'x':-150, 'y':3e2 },
       mapPoint3 = { 'x':-205, 'y':4e2 };
       // Returns the polyline that was passed in, in the specified spatial reference.
       var line = new $geometry.polyline([mapPoint1, mapPoint2, mapPoint3], {wkid: 102100});
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#polygon
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Creates an esri.geometry.Polygon based on the passed in geometry object. If
       * an esri.geometry.Polygon is passed in, it will simply return the same object.
       * @param {Object} geometry An esri polygon or javascript object.
       * @param {Object} spatialReference The spatial reference to set the polygon to.
       * @returns {Object|esri.geometry.Polygon=} An esri.geometry.Polygon.
       *
       * @example
       <pre>
       var mapPoint1 = { 'x':-76, 'y':4e2 };
       mapPoint2 = { 'x':-150, 'y':3e2 };
       mapPoint3 = { 'x':-205, 'y':4e2 };
       mapPoint4 = { 'x':-76, 'y':4e2 };

       // Returns the polygon that was passed in, in the specified spatial reference.
       var line = $geometry.polygon([mapPoint1, mapPoint2, mapPoint3, mapPoint4], {wkid: 102100});
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#xAttr
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Returns the x attribute of the point object.
       * @param {Object} point A point object to retrieve the attribute from.
       * @returns {String} The first attribute name that matched the criteria.
       *
       * @example
       <pre>
       // Returns 'lon'.
       $geometry.xAttr({ 'lon':2, 'lat':45 });

       // Returns 'x'.
       $geometry.xAttr({ 'x':2, 'y':45 });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#yAttr
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Returns the y attribute of the point object.
       * @param {Object} point A point object to retrieve the attribute from.
       * @returns {String} The first attribute name that matched the criteria.
       * @example
       <pre>
       // Returns 'lat'.
       $geometry.yAttr({ 'lat':5, 'lon':6 });

       // Returns 'y'.
       $geometry.yAttr({ 'y':5, 'x':6 });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#xAttrValue
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Returns the value of the x attribute of the point object.
       * @param {Object} point A point object to retrieve the attribute value from.
       * @returns {*} The value of the first attribute name that matched the criteria.
       * @example
       <pre>
       // Returns 5.
       $geometry.xAttrValue({ 'x':5, 'y':6 });

       // Returns 5.
       $geometry.xAttrValue({ 'lon':5, 'lat':6 });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#yAttrValue
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Returns the value of the y attribute of the point object.
       * @param {Object} point A point object to retrieve the attribute value from.
       * @returns {*} The value of the first attribute name that matched the criteria.
       * @example
       <pre>
       // Returns 6.
       $geometry.yAttrValue({ 'x':5, 'y':6 });

       // Returns 6.
       $geometry.yAttrValue({ 'lon':5, 'lat':6 });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#distance
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Returns the distance between two points.
       * @param {Object|Number} point1 One of two points/numbers to find the distance from.
       * @param {Object|Number} point2 Two of two points/numbers to find the distance from.
       * @returns {Number} The distance between the points/numbers.
       * @example
       <pre>
       // Returns 5.
       $geometry.distance(5, 10);

       // Returns 10.8 (plus remainder).
       $geometry.distance({ 'x':2, 'y':5 }, { 'x':8, 'y':-4 });
       // Returns 10.8 (plus remainder).
       $geometry.distance(new esri.geometry.Point(2, 5), new esri.geometry.Point(8, -4));
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#equal
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Determines whether two points or numbers are equal based on a precision.
       * @param {Object|Number} point1 One of two points/numbers to find the distance from.
       * @param {Object|Number} point2 Two of two points/numbers to find the distance from.
       * @param {Number} [delta=.0001] The precision to use when checking if the numbers
       * are equal or not.
       * @returns {Boolean} Whether the points are equal.
       * @example
       <pre>
       // Returns true.
       $geometry.equal(1, 1);
       $geometry.equal({ 'x':2, 'y':5 }, { 'lon':2, 'lat':5 });

       // Returns false.
       $geometry.equal(1, 5);
       $geometry.equal({ 'x':2, 'y':5 }, { 'x':-2, 'y':-5 });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#notEqual
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Determines whether two points or numbers are not equal based on a precision.
       * @param {Object|Number} point1 One of two points/numbers to find the distance from.
       * @param {Object|Number} point2 Two of two points/numbers to find the distance from.
       * @param {Number} [delta=.0001] The precision to use when checking if the numbers
       * are equal or not.
       * @returns {Boolean} Whether the points are not equal.
       * @example
       <pre>
       // Returns false.
       $geometry.notEqual(1, 1);
       $geometry.notEqual({ 'x':2, 'y':5 }, { 'lon':2, 'lat':5 });

       // Returns true.
       $geometry.notEqual(1, 5);
       $geometry.notEqual({ 'x':2, 'y':5 }, { 'x':-2, 'y':-5 });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#extent
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Determines the extent of the feature that was passed in and returns it if it is a valid feature with
       * a valid geometry.  If the feature does not have a valid geometry, undefined is returned.
       * @param {Object} feature The feature from which to determine the extent.
       * @returns {Object} The extent of the feature if it is is found; undefined otherwise.
       * @example
       <pre>
       // Returns extent.
       var feature = new Feature(esri.geometry.Point({x:15,y:20}, layer);
       $geometry.extent(feature);

       // Returns undefined.
       $geometry.extent('test string');
       $geometry.extent(15);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#joinParts
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Concatenates geometries from a featuer into an array.
       * @param {Object} feature The feature from which to extract the geometries.
       * @returns {Array} The geometries from the feature, or an empty array if no geometries are present
       * @example
       <pre>
       // Returns array.
       var feature = new Feature(esri.geometry.Point({x:15,y:20}, layer);
       $geometry.joinParts(feature);

       // Returns empty array.
       var feature = "test string";
       $geometry.joinParts(feature);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#extractMeasures
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Extracts measure values from vertices. See:
       * https://tonicdev.com/jerryharrison/56fb2f1781168d1100abffb0
       * @param {Array} vertices An array of vertices.
       * @returns {Array} An array of flattened "vertices".
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$geometry#pointFromMeasure
       * @methodOf transcend.esri.service:$geometry
       *
       * @description
       * Retrieves a point in a polyline from a given measure
       * @param {Object} polyline The polyline object from which to extract the point
       * @param {Object} measure The route measure from which to determine the corresponding point
       * @param {Object} spatialReference The spatial reference that applies to the polyline
       * @returns {Array} The point corresponding to the given measure if it exists; undefined otherwise.
       * @example
       <pre>
       // Returns point.
       var line = new esri.geometry.Polyline([{x:15,y:20},{x:50,y:25}]);
       var ref = new SpatialReference(wkid: 102100);
       var point = $geometry.pointFromMeasure(line, 5, ref);
       </pre>
       */
/**
 * @ngdoc service
 * @name transcend.esri.$arcgis
 *
 * @description
 * Provides a set of utility functions related to the ArcGIS JavaScript API - particularly
 * {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html 'esri.Map'}
 * related functionality.
 *
 * The main purpose of this service is to provide common functionality
 * related to an {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} object. The '$arcgis' object provides 'static' methods that all take in a
 * 'map' argument, or you can call the factory/provider and you can cache the 'map' object so that you don't have
 * to pass the map to every call.
 *
 <h3>Calling Static Functions</h3>
 <pre>
 $arcgis.someMethod(mapReference, args);
 </pre>

 <h3>Instantiating</h3>
 <pre>
 // The 'map' reference is stored - no need to pass it.
 var util = $arcgis(mapReference);
 util.someMethod(args);
 util.someOtherMethod(otherArgs);
 </pre>

 <h3>Configuration</h3>
 <pre>
 // Configuring via 'esriConfig' object.
 angular.module('myApp').run(function(esriConfig){
   angular.extend(esriConfig, {
    arcgis: {
      geometryUrl: 'http://host/arcgis/foo/GeometryServer'
    }
   });
 });

 // or...

 // Configuring via 'provider'.
 angular.module('myApp').config(function($arcgisProvider){
  angular.extend($arcgisProvider.config, {
    geometryUrl: 'http://host/arcgis/foo/GeometryServer'
  });
 });
 </pre>
 *
 * @param {esri.Map=} cachedMap The map that you want to 'cache' to perform operations against.
 *
 * @requires $rootScope
 * @requires $log
 * @requires $q
 * @requires $object
 * @requires $symbol
 * @requires $geometry
 * @requires dojo
 * @requires esri
 * @requires esriConfig
 */
/**
     * @ngdoc method
     * @name transcend.esri.$arcgis#messages
     * @propertyOf transcend.esri.$arcgis
     *
     * @description
     * Messages that can be configured at the provider configuration time. The following messages are valid:
     * 'valueNoAttributes'.
     * @example
     <pre>
     $arcgisProvider.messages = {
       valueNoAttributes: 'Some other message'
     };
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.esri.$arcgis#config
     * @propertyOf transcend.esri.$arcgis
     *
     * @description
     * Factory configuration object. The following configs are valid: 'geometryUrl'.
     * @example
     <pre>
     $arcgisProvider.config = {
       geometryUrl: 'http://my.server/ArcGIS/rest/services/Geometry/GeometryServer'
     };
     </pre>
     */
/**
       * @name transcend.esri.$arcgis#_layer
       * @description
       * Creates a single layer based on a layer 'configuration'.
       * @param urlOrOptions
       * @param optionsOrType
       * @returns {esri.Layer}
       * @private
       <pre>
       _layer('http://blah', {});
       _layer({url: 'http://blah'});
       _layer('http://blah', 'feature');
       </pre>
       */
/**
       * @name transcend.esri.$arcgis#_layers
       * @description
       * Retrieves a set of esri layers based on a 'configuration' of layers.
       * @private
       * @param {Object} layerConfigs The configuration to use to create the layers from.
       * @returns {Array} The layers created.
       * @example
       <pre>
       var layers = [{
         url: '', options:{}, type:'feature'
       }];
       createLayers(layers);
       </pre>
       */
/**
       * @name transcend.esri.$arcgis#_defer
       * @description
       * Turns an esri/dojo defer to an angular defer.
       * @private
       * @param {dojo.Defer} dojoDefer A dojo defer.
       * @returns {ng.$q.defer} The layers created.
       * @example
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#center
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Gets and/or sets the center location of the passed in map.
       *
       * @param {esri.Map=} map An esri.Map object to get or set the center location.
       * @param {Object=} newCenter A point object used to set the center
       * of the map to. This parameter will also take a true or false value which
       * will determine whether the point will be returned in Web Mercator or not.
       * @param {esri.spatialReference=} spatialReference A spatial reference to set the passed in geometry to.
       * @returns {esri.geometry.Point} The center of the map.
       * @example
       <pre>
       // Gets the center point of the map.
       $arcgis.center(map);
       // or...
       $arcgis(map).center();

       // Sets the center point of the map.
       $arcgis.center(map, { 'x':-76, 'y':42 });
       // or...
       $arcgis(map).center({ 'x':-76, 'y':42 });

       // Sets the center point of the map.
       $arcgis.center(map, new esri.geometry.Point(-76, 42));
       // or...
       $arcgis(map).center(new esri.geometry.Point(-76, 42));
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#graphic
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Creates a graphic from a feature (or similar object). The main point of this functionality
       * is to automatically add any missing components of the graphic - like geometry and symbology.
       *
       * @param {esri.Map=} map An esri.Map object to get or set the center location.
       * @param {esr.geometry.Geometry|esri.Graphic=} feature A feature ({@link https://developers.arcgis.com/en/javascript/jsapi/graphic-amd.html graphic}) or geometry to turn into a valid
       * graphic. The most common use for this is to turn a "feature" from a feature set retrieved from an ArcGIS
       * rest service endpoint into a valid ESRI graphic. The service returns standard JSON objects which need to be
       * turned into ESRI class instances.
       * @param {Object=} options Additional options to use when creating the graphic. These options will also be passed
       * into the {@link transcend.esri.$symbol symbol} factory if a symbol is created.
       * @returns {esri.Graphic} An instance of an {@link https://developers.arcgis.com/en/javascript/jsapi/graphic-amd.html esri.Graphic}.
       * @example
       <pre>
       var graphicObject = {attributes: {}, geometry: { x:1, y: 2 }};
       var esriGraphic = $arcgis.graphic(map, graphicObject, { color: 'blue' });
       // or...
       var esriGraphic = $arcgis(map).graphic(graphicObject, { color: 'blue' });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#graphicsLayer
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Gets or creates a graphics layer depending on whether it exist already. If a graphics layer with the passed in
       * layer id already exists it will simply return that layer - servers as a "get layer by id". If a layer does not
       * already exist with the given id than it will create a new one with the passed in id - assuming the
       * "omitCreation" flag was not passed in as true.
       *
       * @param {esri.Map=} map The {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} to get or create the graphics layer on.
       * @param {Object|String=} layerId The layer or object with 'layerId' property to get or create the new layer from.
       * @param {Boolean} omitCreation If falsy and no layer is found by the passed in layer id, than it will create
       * a new graphics layer with the passed in layer id. If the parameter is true, than it will not create a new layer.
       * @param {Object} options Options that will get passed to the {@link https://developers.arcgis.com/en/javascript/jsapi/graphicslayer-amd.html graphics layer}
       * constructor.
       * @returns {esri.layers.GraphicLayer} The {@link https://developers.arcgis.com/en/javascript/jsapi/graphicslayer-amd.html graphics layer}
       * created or found by the passed in layer id.
       * @example
       <pre>
       var graphicLayer = $arcgis.graphic(map, 'someLayerId');
       // or...
       var graphicLayer = $arcgis(map).graphic('someLayerId');
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#draw
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Draws a {@link https://developers.arcgis.com/en/javascript/jsapi/graphicslayer-amd.html graphic} (feature) or
       * {@link https://developers.arcgis.com/en/javascript/jsapi/geometry-amd.html geometry} on the map - regardless of
       * what type of geometry it is.
       *
       * @param {esri.Map=} map The {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} to
       * draw the graphic on.
       * @param {esr.geometry.Geometry|esri.Graphic=} feature A feature (graphic) or geometry to turn into a valid
       * graphic. The most common use for this is to turn a "feature" from a feature set retrieved from an ArcGIS
       * rest service endpoint into a valid ESRI graphic. The service returns standard JSON objects which need to be
       * turned into ESRI class instances.
       * @param {Object=} opts Additional options to use when creating the graphic. These options will also be passed
       * into the {@link transcend.esri.$symbol symbol} factory if a symbol is created.
       * @returns {esri.Graphic} An instance of an {@link https://developers.arcgis.com/en/javascript/jsapi/graphic-amd.html esri.Graphic}.
       * @example
       <pre>
       var options = {
          zoom: true,          // Auto zooms to graphic.
          clear: true,         // Clears any existing graphics.
          keepVisible: true,   // Makes sure graphic is in viewport.
          expandBy: 1.5   ,    // Zoom out factor.
          graphicsLayer: 'abc' // graphics layer id to create
        };

       var graphic = $arcgis.draw(map, { x: 23, y: 54 }, options);
       // or...
       var graphic = $arcgis(map).draw({ x: 23, y: 54 }, options);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#clearGraphics
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Clears all graphics on a specific graphics layer.
       *
       * @param {esri.Map=} map The {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} to
       * work from.
       * @param {Object|String=} graphicLayerId The layer id of the {@link https://developers.arcgis.com/en/javascript/jsapi/graphicslayer-amd.html graphics layer}
       * to clear. This can also be an object with a 'layerId'
       * property on it.
       * @returns {transcend.esri.$arcgis} A reference back to {@link transcend.esri.$arcgis $arcgis} instance ("this") - for chaining.
       * @example
       <pre>
       var graphic = $arcgis.clearGraphics(map, 'someLayerId');
       // or...
       var graphic = $arcgis(map).clearGraphics({ layerId: 'someLayerId' });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#layerUrl
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Retrieves a layer's url by either a layer object or id. In addition, if a url
       * is passed in it will simply return the same url as a safe-guard.
       *
       * @param {esri.Map=} map The {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} to
       * work from.
       * @param {String} layerId The layer id of the layer to retrieve its URL from.
       * @returns {String|undefined} The url of the specified layer.
       * @example
       <pre>
       var url = $arcgis.layerUrl(map, 'someLayerId');
       // or...
       var url = $arcgis(map).layerUrl('someLayerId');
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#layer
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Retrieves a map layer either by 'id' or index.
       *
       * @param {esri.Map=} map The {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} to
       * work from.
       * @param {String|Number} layerId The layer id, service url, or index, of the layer to retrieve from the map.
       * @returns {Object} A map layer.
       * @example
       <pre>
       var layer = $arcgis.layer(map, 'someLayerId');
       // or...
       var layer = $arcgis.layer(map, 2);
       // or...
       var layer = $arcgis.layer(map, 'http://myserver/arcgis/rest/services/a/MapServer/3');
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#identify
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Performs a map {@link https://developers.arcgis.com/en/javascript/jsapi/identifytask-amd.html identify task}
       * from a given point.
       *
       * @param {esri.Map=} map The {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} to
       * work from.
       * @param {esri.geometry.Point|Object} mapPoint The point at which to perform the identify. This point can be an
       * {@link https://developers.arcgis.com/en/javascript/jsapi/point-amd.html esri point} or an object with point
       * properties (x, y, lat, lon, etc).
       * @param {esri.layers.Layer|String} layerOrUrl The layer, or the URL of a layer, to perform the identify against.
       * @param {Object=} options Options that will get passed to the {@link https://developers.arcgis.com/en/javascript/jsapi/identifytask-amd.html identify task}
       * when performing the action.
       * @returns {Promise} A {@link http://docs.angularjs.org/api/ng.$q promise} that will be resolved when the
       * identify completes.
       * @example
       <pre>
       var point = $geometry.point(mapPoint, map.spatialReference);
       var options = {
         tolerance: 4,
         returnGeometry: true,
         returnM: true,
         layerOption: esri.tasks.IdentifyParameters.LAYER_OPTION_VISIBLE
       };

       var promise = $arcgis.identify(map, point, 'layerId2', options);
       // or...
       var promise = $arcgis(map).identify(point, 'layerId2', options);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#addSelection
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Creates selection functionality for a given layer and/or sub layers (if its an {@link https://developers.arcgis.com/en/javascript/jsapi/arcgisdynamicmapservicelayer-amd.html ArcGISDynamicMapServiceLayer}).
       * The selection options allow you to automatically draw a {@link https://developers.arcgis.com/en/javascript/jsapi/graphic-amd.html graphic}
       * (feature), zoom to, and/or highlight when selected. A "callback" property can also be called so that custom
       * actions can take place when selection happens.
       *
       * @param {esri.Map=} map The {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} to
       * work from.
       * @param {esri.layers.Layer|String} layerOrUrl The layer, or the URL of a layer, to add selection to.
       * @param {Array|String=} layerIds An array of layer ids or comma sepearted string of layer ids to add selection to.
       * This specifically refers to the sub layer ids of an {@link https://developers.arcgis.com/en/javascript/jsapi/arcgisdynamicmapservicelayer-amd.html ArcGISDynamicMapServiceLayer}.
       * @param {Object=} options Options to override/change behaviour.
       * @returns {*} A standard {@link https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent?redirectlocale=en-US&redirectslug=DOM%2FMouseEvent DOM MouseEvent}
       * with additional properties such as {@link https://developers.arcgis.com/en/javascript/jsapi/point-amd.html mapPoint}
       * and {@link https://developers.arcgis.com/en/javascript/jsapi/point-amd.html screenPoint}. See {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html#event-click here}
       * for more details.
       * @example
       <pre>
       var layerUrl = 'http://host/arcgis/rest/services/test/MapServer';
       var layerIds = [1,3,5];
       var options = {
        onClick: function () {},   // Called on map click.
        onIdentify: function () {} // Called on identify complete.
       };

       var handler = $arcgis.addSelection(map, layerUrl, layerIds, options);
       // or...
       var handler = $arcgis(map).addSelection(layerUrl, layerIds, options);

       // Can remove selection (map click):
       handler.remove();
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#addLayers
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Creates and adds the layers from a layer 'configuration' to the passed in map.
       *
       * @param {esri.Map=} map The {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} to
       * add the layers to.
       * @param {Object=} layersConfig A configuration object to create layers by.
       * @param {Function=} onBeforeAddedToMap A function that will fire before configured layers are added to the map.
       * @returns {Array} The list of layers created based on the passed in configuration.
       * @example
       <pre>
       var layersConfig =[
       {
         url: 'http://host/arcgis/server/foo/MapServer'
       },
       {
         url: 'http://host/arcgis/server/foo/MapServer/3',
         type: 'feature'
       }];

       var layers = $arcgis.addLayers(map, layersConfig);
       // or...
       var layers = $arcgis(map).addLayers(layersConfig);
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#project
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Projects a geometry to the map's spatial reference. This is done by using a {@link https://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer geometry service}.
       * The default {@link https://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer geometry service}
       * is ESRI's arcgis online service. This geometry service url can be configured using "esriConfig" or via the
       * $arcgisProvider.
       *
       * @param {esri.Map=} map The {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} to
       * work from.
       * @param {esri.geometry.Geometry|Object} geometry An @link https://developers.arcgis.com/en/javascript/jsapi/geometry-amd.html esri geometry}
       * or object representation of a geometry to convert to different spatial reference.
       * @returns {ng.$q.defer} A promise that will resolve when the conversion is complete.
       * @example
       <pre>
       var geometry = new esri.geometry.Point({ x: 1, y: 2 }, { wkid: 1234 });

       var promise = $arcgis.project(map, geometry);
       // or...
       var promise = $arcgis(map).project(geometry);

       promise.then(function(projectedGeometry) {
         expect(projectedGeometry.spatialReference.wkid)
           .toEqual(map.spatialReference.wkid);
       });
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$arcgis#extent
       * @methodOf transcend.esri.$arcgis
       *
       * @description
       * Gets or sets the map's extent. When setting the extent, if the extent is not in the same projection as the
       * map it will automatically project it to the map's sptial reference - using the {@link transcend.esri.$arcgis#project project}
       * method.
       *
       * @param {esri.Map=} map The {@link https://developers.arcgis.com/en/javascript/jsapi/map-amd.html esri map} to
       * get or set the extent from/to.
       * @param {esri.geometry.Extent} extent The {@link https://developers.arcgis.com/en/javascript/jsapi/extent-amd.html extent}
       * to set the map to.
       * @returns {esri.geometry.Extent|ng.$q.defer} The map's {@link https://developers.arcgis.com/en/javascript/jsapi/extent-amd.html extent}
       * if no extent parameter was passed in, or a promise if an extent setter was passed in.
       * @example
       <pre>
       var extent = $arcgis.extent(map);
       // or...
       var extent = $arcgis(map).extent();
       </pre>
       */
/**
 * @ngdoc service
 * @name transcend.esri:$$layer
 *
 * @description
 The '$$layer' factory
 *
 *
 * @requires $protoFactory
 * @requires $array
 *
 */
/**
       * @ngdoc method
       * @name transcend.esri.$layer#field
       * @methodOf transcend.esri.$layer
       *
       * @description
       * Gets a field by name for a layer.
       *
       * @param {object=} layer An object with a 'fields' property on it.
       * @param {string=} fieldName The name of the field to get the field by. If this parameter is not passed in it
       * will look for a 'displayField' or 'defaultField' on the layer object.
       * @returns {object} The field with the passed in field name or undefined if it can not be found.
       * @example
       <pre>
       var field = $layer.field({ fields: [{name: 'Field1', alias:'One'}, {name: 'Field2', alias:'Two'}] }, 'Field2');
       expect(field.alias).toEqual('Two');

       // or...
       var lyr = $layer({ displayField: 'Field2', fields: [{name: 'Field1', alias:'One'}, {name: 'Field2', alias:'Two'}] });
       expect(lyr(field('Field2').alias).toEqual('Two');
       expect(lyr(field().alias).toEqual('Two');
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$layer#domain
       * @methodOf transcend.esri.$layer
       *
       * @description
       * Gets a coded value domain based on a value.
       *
       * @param {object=} layer An object with a 'fields' property on it.
       * @param {string=} fieldName The name of the field to get the field by. If this parameter is not passed in it
       * will look for a 'displayField' or 'defaultField' on the layer object.
       * @param {*=} fieldValue The value to lookup the "alias" (lookup) from in the coded value domains.
       * @returns {object} The coded value domain object that matched the given value.
       * @example
       <pre>
       var layer = { fields: [{name: 'Field1', alias:'One'}, {name: 'Field2', alias:'Two', domain: {codedValues: [{name: 'Right', code: 'R'}]}}] }
       var domain = $layer.domain(layer, 'Field2', 'R');
       expect(domain.name).toEqual('Right');
       expect(domain.code).toEqual('R');

       // or...
       var domain = $layer(layer).domain('Field2', 'R');
       expect(domain.name).toEqual('Right');
       expect(domain.code).toEqual('R');
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.$layer#domainValue
       * @methodOf transcend.esri.$layer
       *
       * @description
       * Gets a coded value domain's alias value (lookup value).
       *
       * @param {object=} layer An object with a 'fields' property on it.
       * @param {string=} fieldName The name of the field to get the field by. If this parameter is not passed in it
       * will look for a 'displayField' or 'defaultField' on the layer object.
       * @param {*=} fieldValue The value to lookup the "alias" (lookup) from in the coded value domains.
       * @returns {object} The value of the coded value domain object that matched the given value. If no coded value
       * domain was found that matched the value the original value that was passed in will be returned.
       * @example
       <pre>
       var layer = { fields: [{name: 'Field1', alias:'One'}, {name: 'Field2', alias:'Two', domain: {codedValues: [{name: 'Right', code: 'R'}]}}] }
       var domain = $layer.domain(layer, 'Field2', 'R');
       expect(domain).toEqual('Right');

       // or...
       var domain = $layer(layer).domain('Field2', 'R');
       expect(domain).toEqual('Right');
       </pre>
       */
/**
 * @ngdoc service
 * @name transcend.esri.$layer
 *
 * @description
 * Provides a set of utility functions related to a plain layer object (not an instantiated object).
 *
 * The main purpose of this service is to provide common functionality that will be performed on a layer object.
 *
 <h3>Calling Static Functions</h3>
 <pre>
 $layer.someMethod(layerReference, args);
 </pre>

 <h3>Instantiating</h3>
 <pre>
 // The 'layer' reference is stored - no need to pass it.
 var lyr = $layer(layerReference);
 lyr.someMethod(args);
 lyr.someOtherMethod(otherArgs);
 </pre>
 *
 * @param {object=} layer The layer object to 'cache' to perform operations against.
 *
 * @requires transcend.core.$protoFactory
 * @requires $array
 */
/**
 * @ngdoc directive
 * @name transcend.esri.directive:esriMap
 *
 * @description
 * The 'esriMap' directive allows the creation of ESRI maps using the ArcGIS JavaScript API in markup and
 * angular ways.
 *
 * Note: When using this directive you must include the ESRI ArcGIS JavaScript API - both JavaScript
 * and CSS files.
 *
 * @restrict EA
 * @element ANY
 * @scope
 *
 * @requires $timeout
 * @requires $q
 * @requires $window
 * @requires transcend.core#esriConfig
 * @requires dojo
 * @requires transcend.esri#esri
 * @requires transcend.esri#$arcgis
 *
 * @param {object=} map A map object that is bound two ways. This map object can be empty or it can be an
 * already instantiated 'esri' map object. If it is already instantiated, that map object will be used
 * throughout the directive. If the map is null/nothing, or not passed in, than the directive will
 * create a new 'esri' map instance.
 * @param {number=} zoom A zoom property that is bound two ways.
 * @param {object=} center A center object that is bound two ways. This center object supports the following x
 * coordinate fields: 'x,lon,lng,longitude,ln' and the following y coordinate fields: 'y,lat,latitude,lattitude,lt'.
 * @param {boolean=} loading A flag that states whether the map is currently loading (updating content) or not.
 * @param {string=} layers A layer configuration that will be used to setup the layers for the map.
 * TODO: Currently this is a string but we need to change this to be an object.
 * @param {number=} x The 'x' coordinate to start the map's center location at. Note, if a center parameter is
 * passed in than this attribute will be ignored.
 * @param {number=} y The 'y' coordinate to start the map's center location at. Note, if a center parameter is
 * passed in than this attribute will be ignored.
 * @param {string=} width The width to make the map.
 * @param {string=} height The width to make the map.
 * @param {string=} cacheKey If the "cache key" is set, than the map will store user changes per the map, and persist
 * them to other sessions - things like the map extent, zoom level, etc.
 * @param {string=} cacheEvent The map event on which to save the session information - if a cache key is present.
 * @param {string=} basemap The basemap to use on the map. The ESRI map will except any of the following:
 * "streets" , "satellite" , "hybrid", "topo", "gray", "oceans", "national-geographic", "osm".
 * @param {string=} zoomToLayer The layer id that you want the map to initially zoom to. This requires that the
 * "layers" configuration object be populated with a layer and that layer id will correspond to this property.
 * @param {function=} onReady Callback function for when the map is ready. It will pass the 'map' object as an
 * argument. Note, this does not guarantee that the layer infos have been loaded yet, only the map.
 * @param {function=} onLayersReady Callback function for when all layers have been loaded. This callback
 * &param {function=} onBeforeLayersAdded Function fired before layers area actually added to the map.
 * guarantees that the layer infos have been loaded for all layers.
 * @example
 <example module="app">
 <file name="index.html">
 <link rel="stylesheet" href="//js.arcgis.com/3.7/js/esri/css/esri.css">
 <div ng-controller="Ctrl">
 <h1>Simple Map</h1>
 <esri-map center="center" zoom="zoom"></esri-map>

 <hr>
 <label>
 X Coordinate:
 <input type="number" ng-model="center.x" step="10000" />
 </label>
 <label>
 Y Coordinate:
 <input type="number" ng-model="center.y" step="10000" />
 </label>
 <label>
 Zoom Level:
 <input type="number" ng-model="zoom" />
 </label>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.esri'])
 .controller('Ctrl', function($scope) {
        $scope.center = { x: -78, y: 40 };
        $scope.zoom = 7;
      });
 </file>
 </example>
 */
/**
 * @ngdoc service
 * @name transcend.esri.provider:MapServer
 *
 * @description
 * The 'MapServer' provider
 *
 * @requires $$mapConfig
 */
/**
         * Function to get the gdb version to be passed to the map service call
         * If target version is the same as the layer version, return an empty string. If the target version is not
         * passed than the "SDE.DEFAULT" version will be applied as the target.
         * @returns {string}
         */
/**
 * @ngdoc service
 * @name transcend.esri.provider:QueryDefinition
 *
 * @description
 * The 'QueryDefinition' service
 *
 * @requires $resource
 * @requires transcend.esri.esriConfig
 */
/**
 * @ngdoc directive
 * @name transcend.esri.directive:featureSelection
 *
 * @description
 * The 'featureSelection' directive provides a subset of tools to enable the selection of features for the specified map layers and enables
 * highlighting of selected features.
 *
 * @restrict EA
 * @element ANY
 * @replace true
 *
 * @requires esriConfig
 * @requires $timeout
 * @requires $array
 * @requires $arcgis
 * @requires $filter
 * @requires transcend.core.$notify
 * @requires esri
 * @requires dojo
 *
 * @param {string} basemap The basemap active for the current session. Valid options are: "hybrid", "streets",
 * "satellite", "topo", "gray", "oceans", "national-geographic", "osm"
 *
 * @param {function} onChange The method used to change the basemap in the map display.
 *
 * @example
 <example module="app">
 <file name="index.html">
 <link rel="stylesheet" href="http://js.arcgis.com/3.7/js/esri/css/esri.css">
 <div ng-controller="Ctrl">
 <esri-map center="center" zoom="zoom" style="position:relative">
 <div style="position:absolute; left: 10px; bottom: 10px;">
 <basemap-selector basemap="selection.basemap"></basemap-selector>
 </div>
 </esri-map>

 <hr>
 <label>
 Basemap:
 <select ng-model="selection.basemap" ng-options="bm for bm in basemaps"></select>

 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.esri'])
 .controller('Ctrl', function($scope) {
        $scope.selection = {};
        $scope.basemaps = ['hybrid', 'streets', 'satellite', 'topo', 'gray', 'oceans', 'national-geographic', 'osm'];
      });
 </file>
 </example>
 */
/**
 * @ngdoc service
 * @name transcend.esri.service:$symbol
 *
 * @description
 * Provides a quick and abstract method of creating ESRI symbology. The main use case for this service is
 * to set the default colors you want in the configuration and then simply call this service to generate
 * symbol class instances without having to recreate them each time.
 *
 * Note: When using this service you must include the ESRI ArcGIS JavaScript API.
 *
 * @requires dojo
 * @requires esri
 *
 <h3>Calling Static $symbol Utility Functions</h3>
 <pre>
 // Create a line symbol
 var line = $symbol.polyline({polylineWidth: 2, polylineColor: [221, 011, 150]});

 //Create a point symbol
 var ref = $symbol.point({pointColor:[255, 0, 0], pointSize: 30});

 //Create a polygon symbol
 var ref = $symbol.polygon({polygonColor: [255, 100, 100], polygonOutlineWidth: 4});
 </pre>
 *
 * See {@link http://docs.angularjs.org/api/ngResource/service/$resource AngularJS's $resource} documentation for
 * details on what methods are natively available to a resource.

 */
/**
       * @ngdoc method
       * @name transcend.esri.service:$symbol
       * @methodOf transcend.esri
       *
       * @description
       * Create a symbol for the geometry type found in the feature, using the options to set the symbol's attributes
       * @param {Object} feature  The feature from which to get the geometry type.
       * @param {Object} options The list of attributes to apply to the symbol
       * @returns {Object} The new symbol.
       *
       * @example
       <pre>
       // Returns a symbol with the designated color for a polyline
       var line = new esri.geometry.Polyline();
       var feature = mew Feature(line, layer);
       var symbol = $symbol.get(feature, {polylineColor: [255,255,0]});
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$symbol#polygon
       * @methodOf transcend.esri.service:$symbol
       *
       * @description
       * Creates an esri polygon symbol based on the passed in options.
       * @param {Array} opts The list of attributes to be applied to the symbol
       * @returns {Object} An esri symbol that represents a polygon
       *
       * @example
       <pre>
       // Returns a symbol for a  polygon
       $symbol.polygon({polygonOutlineWidth: 3, polygonColor: [255, 035, 200]});
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$symbol#polyline
       * @methodOf transcend.esri.service:$symbol
       *
       * @description
       * Creates an esri polyline symbol based on the passed in options.
       * @param {Array} opts The list of attributes to be applied to the symbol
       * @returns {Object} An esri symbol that represents a polyline
       *
       * @example
       <pre>
       // Returns a symbol for a  polyline
       $symbol.polyline({polylineWidth: 4, polylineColor: [140, 035, 150]});
       </pre>
       */
/**
       * @ngdoc method
       * @name transcend.esri.service:$symbol#point
       * @methodOf transcend.esri.service:$symbol
       *
       * @description
       * Creates an esri point symbol based on the passed in options.
       * @param {Array} opts The list of attributes to be applied to the symbol
       * @returns {Object} An esri symbol that represents a point
       *
       * @example
       <pre>
       // Returns a symbol for a  point
       $symbol.point({pointColor:[255, 50, 100], pointSize: 25});
       </pre>
       */
/**
 * @ngdoc service
 * @name transcend.esri.service:DrawingInfo
 *
 * @description
 * The 'DrawingInfo' factory returns methods
 *
 * @requires transcend.core.$string
 * @requires $class
 * @requires $color
 *
 **/
